import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(Parameterized.class)
public class CalculatorTestParametrized {

    private int number;

    public CalculatorTestParametrized(int number) {
        this.number = number;
    }


    @Parameterized.Parameters(name = "{index} isEven: {0}")
    public static Iterable<?> data() {
//        return Arrays.asList(2, 4, 6, 8, 10, 12, 14, 26, 56);

        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 500; i+=2) {
            list.add(i);
        }
        return list;

    }


    @Test
    public void isEven(){
        Assert.assertTrue(Calculator.isEven(this.number));
    }


}
