import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

@RunWith(Parameterized.class)
public class CalculatorTestSumParametrized {

    @Parameterized.Parameter
    public int a;

    @Parameterized.Parameter(1)
    public int b;

    @Parameterized.Parameter(2)
    public int expected;


    public CalculatorTestSumParametrized() {
    }

    @Parameterized.Parameters(name = "{index} sum a {0} and b {1} expected {2}")
    public static Iterable<? extends Object[]> data() {

        return Arrays.asList(new Object[][]{
                {1, 2, 3},
                {2, 5, 7},
                {-1, -2, -3},
                {10, 100, 110}
        });
    }

    @Test
    public void sum_parametrized() {
        Assert.assertEquals(Calculator.sum(a, b), expected);
    }

}
