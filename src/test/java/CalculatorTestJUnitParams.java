import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;

@RunWith(JUnitParamsRunner.class)
public class CalculatorTestJUnitParams {

    public CalculatorTestJUnitParams() {
    }


    @Test
    @Parameters({"10,20,30", "40,50,90"})
    public void sum_parametrized(int a, int b, int expected) {
        Assert.assertEquals(Calculator.sum(a, b), expected);
    }

    @Test
    @Parameters(method = "getData")
    @TestCaseName("{index} sum a {0} b {1} expected {2}")
    public void sumMethodParamtrized(int a, int b, int expected) {
        Assert.assertEquals(Calculator.sum(a, b), expected);
    }

    public Object[][] getData() {
        return new Object[][]{
                {1, 2, 3},
                {3, 4, 7},
                {-1, -2, -3}
        };
    }
}