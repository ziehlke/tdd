import org.junit.BeforeClass;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

public class CalculatorTestsAssertJ {


    private static Calculator calculator;

    @BeforeClass
    public static void setUp() {
        calculator = new Calculator();
    }


    @Test
    public void multiply_6_7_42(){
        assertThat(calculator.multiply(6,7)).isEqualTo(42);
    }

    @Test
    public void sum_6_7_13(){
        assertThat(calculator.sum(6,7)).isEqualTo(13);
    }

    @Test
    public void difference_13_7_6(){
        assertThat(calculator.difference(13,7)).isEqualTo(6);
    }

    @Test
    public void divide_1_0_IllegalArgumentException() {
        assertThatIllegalArgumentException()
                .isThrownBy(()->{Calculator.divide(1,0);});
    }




}
