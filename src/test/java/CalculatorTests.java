import org.junit.*;
import org.junit.rules.ExpectedException;


public class CalculatorTests {

    private static Calculator calculator;

    @BeforeClass
    public static void setUp() {
        calculator = new Calculator();
    }


    @Test
    public void sum_a6b7_13() {
        int expected = 13;

        int actual = calculator.sum(6, 7);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void multiply_a6b7_42() {
        int expected = 42;

        int actual = calculator.multiply(6, 7);

        Assert.assertEquals(expected, actual);
    }


    @Test
    public void difference_a13b7_6() {
        int expected = 6;

        int actual = calculator.difference(13, 7);

        Assert.assertEquals(6, actual);
    }

    @Test
    public void divide_a42b7_6() {
        int expected = 6;

        int actual = calculator.divide(42, 7);

        Assert.assertEquals(6, actual);
    }


    @Test(expected = IllegalArgumentException.class)
    public void divide_1_0_IllegalArgumentException() {
        new Calculator().divide(1, 0);
    }

    @Test
    public void devide_5_0_IllegalArgumentException() {

        try {
            Calculator.divide(5, 0);
            Assert.fail("Should thrown IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            return;
        }
    }



    // 3 spoosby na wychwycenie wyjatku
    // try / catch
    // expected w Test
    // oraz @Rule

    @Test (expected = IllegalArgumentException.class)
    public void divide_10_0_IllegalArgumentException(){
        Calculator.divide(10,0);
    }



    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void divide_20_0_IllgalArgumentException() {
        thrown.expect(IllegalArgumentException.class);
        Calculator.divide(20,0);
    }


    @Test
    public void isEven_2_true() {
        Assert.assertTrue(Calculator.isEven(2));
    }


    @AfterClass
    public static void tearDown() {
        calculator = null;
    }

}
