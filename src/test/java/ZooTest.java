import org.junit.*;
import org.junit.rules.ExpectedException;

public class ZooTest {

    Zoo zoo;
    Pet pet;

    @Before
    public void setUp() {
        zoo = new Zoo();
        pet = new Pet();
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test(expected = NullPointerException.class)
    public void getPetNames_null_NullPointerException() {
        zoo.giveHomelessPet(null);
        zoo.getPetNames();
    }


    @Test
    public void getPetNames_defaultPet_StringNull() {
        String expected = "null";
        zoo.giveHomelessPet(new Pet());
        String actual = zoo.getPetNames();

        Assert.assertEquals(expected, actual);
    }


    @Test
    public void getPetNames_PetWithNameFafik_Fafik() {
        String expected = "Fafik";

        Pet fafik = new Pet();
        fafik.setName("Fafik");
        zoo.giveHomelessPet(fafik);
        String actual = zoo.getPetNames();
        Assert.assertEquals(expected, actual);
    }


    @Test
    public void getPetNames_oneOfPetsIsNull_NullPointerException() {
        thrown.expect(NullPointerException.class);

        pet.setName("Fafik");
        zoo.giveHomelessPet(pet);
        zoo.giveHomelessPet(null);
        zoo.getPetNames();
    }


    @Test
    public void getPetNames_takePetDefaultZoo_emptyString() {
        zoo.takePet(pet);
        String expected = "";
        Assert.assertEquals(expected, zoo.getPetNames());
    }


    @Test
    public void getPetNames_giveHomelessPetTakePetDefaultZoo_emptyString() {

        zoo.giveHomelessPet(pet);
        zoo.takePet(pet);

        Assert.assertEquals("", zoo.getPetNames());
    }

    @Test
    public void getPetNames_giveHomelessPetWithNameFafikAndTakeDifferentPet_Fafik() {

        Pet fafik = new Pet();
        fafik.setName("Fafik");

        zoo.giveHomelessPet(fafik);
        zoo.takePet(pet);

        Assert.assertEquals("Fafik", zoo.getPetNames());
    }

    @Test
    public void getPetNames_giveTwoTheSamePetsWithNameFafikAndTakeOneAndAddAnother_Fafiknull() {

        Pet fafik = new Pet();
        fafik.setName("Fafik");

        zoo.giveHomelessPet(fafik);
        zoo.giveHomelessPet(fafik);
        zoo.giveHomelessPet(pet);
        zoo.takePet(fafik);

        Assert.assertEquals("Fafiknull", zoo.getPetNames());
    }


    @Test(expected = NullPointerException.class)
    public void getPetNames_GivePetWithNameDefaultPetPetWithNameAndTakeDefaultAndTakeNamed_NullPointerException() {

        Pet fafik = new Pet();
        fafik.setName("Fafik");

        zoo.giveHomelessPet(fafik);
        zoo.giveHomelessPet(null);
        zoo.giveHomelessPet(fafik);
        zoo.giveHomelessPet(null);

        zoo.takePet(pet);
        zoo.takePet(null);

        zoo.getPetNames();
    }


    @Test
    public void getPetNames_GetNullPet_EmptyString() {
        zoo.takePet(null);
        Assert.assertEquals("", zoo.getPetNames());
    }


}
