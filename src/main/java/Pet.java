public class Pet {
    private String name;
    private double weight;
    private double height;
    private int age;

    private static final double MINWEIGHT = 1.0;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBmi() {
        if(this.height <= 0){
            throw new IllegalStateException("Height can't be less or equal 0");
        }

        if(this.weight <= 0){
            throw new IllegalStateException("Weight can't be equal 0");
        }

        return this.weight/(this.height*this.height);
    }

    public void feed(double amountOfFood) {
        if(amountOfFood <= 0)
        {
            throw new IllegalArgumentException("You can't feed pet with nothing it can be angry");
        }

        this.weight += amountOfFood;
    }

    public void sleep() {
        if (isWeightToLow(this.weight)) throw new IllegalStateException("Weight to low to sleep");
        this.weight -= MINWEIGHT;
        this.height += 0.1;
    }

    private boolean isWeightToLow(double weight){
        return weight <= MINWEIGHT;
    }
}