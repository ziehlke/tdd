import java.util.ArrayList;
import java.util.List;

public class Zoo {
    private List<Pet> petPlaces;

    public Zoo() {
        this.petPlaces = new ArrayList<Pet>();
    }

    public void giveHomelessPet(Pet pet){
        this.petPlaces.add(pet);
    }

    public void takePet(Pet pet){
        this.petPlaces.remove(pet);
    }

    public String getPetNames(){
        String aggregator = "";
        for (Pet pet:
             petPlaces) {
            aggregator += pet.getName();
        }

        return aggregator;
    }
}
